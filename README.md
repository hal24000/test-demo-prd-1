# StUI

Basic template to create a Streamlit app for analysis, visualization and deployment via Dimension Dashboard.

## Installation

Use Dimensionops to create a project, using the stui template.

```bash
$ dimensionops create-project
$ your_project_name
$ .
$ stui
```

## Usage
```bash
$ cd your_project_name/src
$ streamlit run app.py
```


## Project Organisation

    ├── chart                   <- Files required for Dimensionops build
    │
    ├── data                    <- Data (to be kept locally)
    │
    ├── notebooks               <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                              the creator's initials, and a short `-` delimited description, e.g.
    │                              `1.0-KK-Initial-data-exploration`
    │
    ├── src                     <- Source code for project
    │   │
    │   │── features            <- Features for models
    │   │
    │   │── setup               <- Basic setup files
    │   │   └── layout.py       <- Basic layout of Streamlit app
    │   │ 
    │   └── visualization       <- Visualizations
    │       └── plot.py         <- Simple plot
    │ 
    │── Dockerfile              <- File to assemble a Docker image
    │
    │── env.yml                 <- Environment yml file to create conda environment
    │
    ├── README.md               <- README for this project
    │
    └── requirements.txt        <- Requirements file for creating app environment